FROM golang:latest

WORKDIR /application

COPY ./app /application

RUN go mod download

RUN go get github.com/githubnemo/CompileDaemon

RUN go get github.com/Syfaro/telegram-bot-api

ENTRYPOINT CompileDaemon --build="go build -o ./src/build" --command=./src/build

