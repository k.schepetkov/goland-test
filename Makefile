bold := $(shell tput bold)
sgr0 := $(shell tput sgr0)
.DEFAULT_GOAL := help

help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

build: ## Build containers
	docker-compose up -d --build
	@echo "\n $(bold)Build successfully.$(sgr0) \n"

up: ## Up containers
	docker-compose up -d --force-recreate --remove-orphans

down: ## Down containers
	@docker-compose down

stop: ## Stop contrainers
	@docker-compose stop

restart: stop up ## Restart docker containers

go-console: ## PHP console
	docker-compose exec app bash

go-run: ## Go test
	@docker-compose exec app sh -c 'go run main.go'