package youtube

import (
	"encoding/json"
	"go.test/local/model"
	"math/rand"
	"net/http"
	"os"
	"time"
)

const DefaultWordEng string = "Top"
const DefaultWordRus string = "популярное"

func GetRandomVideo() (*model.ResponseYouTube, error) {
	var word = getRendomWord()

	var youtubeUrl = "https://www.googleapis.com/youtube/v3/search?key=" + os.Getenv("YOUTUBE_KEY") + "&maxResults=1&part=snippet&type=video&q=" + word

	resp, err := http.Get(youtubeUrl)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		youtubeResp := &model.ResponseYouTube{}
		json.NewDecoder(resp.Body).Decode(youtubeResp)
		return youtubeResp, nil
	}

	return nil, err
}

func getRendomWord() string {
	if randomInt() >= 5 {
		return getRandomWordEng()
	} else {
		return getRandomWordRus()
	}
}

func randomInt() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(10)
}

func getRandomWordEng() string {
	resp, err := http.Get("http://api.wordnik.com:80/v4/words.json/randomWords?hasDictionaryDef=true&minCorpusCount=0&minLength=5&maxLength=15&limit=1&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5")
	if err != nil {
		return DefaultWordEng
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		wordEngResp := make([]model.ResponseEndWord, 0)
		json.NewDecoder(resp.Body).Decode(&wordEngResp)
		return wordEngResp[0].Word
	} else {
		return DefaultWordEng
	}
}

func getRandomWordRus() string {
	resp, err := http.Get("http://free-generator.ru/generator.php?action=word&type=0")
	if err != nil {
		return DefaultWordRus
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		wordRusResp := &model.ResponseRusWord{}
		json.NewDecoder(resp.Body).Decode(wordRusResp)
		return wordRusResp.Word.Word
	} else {
		return DefaultWordRus
	}
}
