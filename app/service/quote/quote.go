package quote

import (
	"encoding/json"
	"errors"
	"go.test/local/model"
	"net/http"
)

func GetQuote() (*model.QuoteData, error) {
	resp, err := http.Get("https://api.forismatic.com/api/1.0/?method=getQuote&format=json&json=parseQuote")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		quoteResp := &model.QuoteData{}
		json.NewDecoder(resp.Body).Decode(quoteResp)
		return quoteResp, nil
	}

	return nil, errors.New("Could not get quote from API")
}
