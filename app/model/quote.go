package model

type QuoteData struct {
	Text       string `json:"quoteText"`
	Author     string `json:"quoteAuthor"`
	Name       string `json:"senderName"`
	SenderLink string `json:"senderLink"`
	Link       string `json:"quoteLink"`
}
