package model

type ResponseEndWord struct {
	Id   int    `json:"id"`
	Word string `json:"word"`
}
