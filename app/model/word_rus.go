package model

type ResponseRusWord struct {
	Word WordData `json:"word"`
}

type WordData struct {
	Id   string `json:"id"`
	Word string `json:"word"`
	Type string `json:"type"`
}
