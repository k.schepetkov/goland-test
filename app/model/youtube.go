package model

type ResponseYouTube struct {
	Items []ItemsVideo `json:"items"`
}

type ItemsVideo struct {
	Id IdVideo `json:"id"`
}

type IdVideo struct {
	VideoId string `json:"videoId"`
}
