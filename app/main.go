package main

import (
	"github.com/Syfaro/telegram-bot-api"
	"go.test/local/handler"
	"log"
	"os"
	"strconv"
)

func getBot() *tgbotapi.BotAPI {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_TOKEN"))
	if err != nil {
		log.Panic(err)
	}

	debug, _ := strconv.ParseBool(os.Getenv("DEBUG"))
	bot.Debug = debug

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	return bot
}

func main() {
	var bot = getBot()

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, _ := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}
		log.Printf("Request: [%s] %s", update.Message.From.UserName, update.Message.Text)
		var msg tgbotapi.MessageConfig

		switch update.Message.Text {
		case "/start":
			msg = handler.IndexHandler(update.Message.Chat.ID)
		case "/quote":
			msg = handler.QuoteHandler(update.Message.Chat.ID)
		case "/youtube":
			msg = handler.YoutubeHandler(update.Message.Chat.ID)
		default:
			msg = handler.DefaultHandler(update.Message.Chat.ID)
		}

		log.Printf("Response: [%s] %s", update.Message.From.UserName, msg.Text)
		bot.Send(msg)
	}
}
