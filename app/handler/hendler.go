package handler

import (
	"github.com/Syfaro/telegram-bot-api"
	"go.test/local/helper"
	"go.test/local/service/quote"
	"go.test/local/service/youtube"
	"os"
)

func IndexHandler(chatID int64) tgbotapi.MessageConfig {
	return tgbotapi.NewMessage(chatID, "О, привет. Выбирай команду и погнали. \n\n"+helper.GetCommand())
}

func QuoteHandler(chatID int64) tgbotapi.MessageConfig {
	quoteResp, err := quote.GetQuote()
	if err != nil {
		return tgbotapi.NewMessage(chatID, "Упс... Я не смог получить цитату дня, попробуй заного или чуть позже.")
	}

	var message = quoteResp.Text

	if quoteResp.Author != "" {
		message += "\n\n © " + quoteResp.Author
	}

	return tgbotapi.NewMessage(chatID, message)
}

func YoutubeHandler(chatID int64) tgbotapi.MessageConfig {
	youtubeResp, err := youtube.GetRandomVideo()
	var message string

	if err != nil {
		message = "Упсс, мы не смогли получить видео из YouTube."
	} else {
		message = os.Getenv("YOUTUBE_URL") + youtubeResp.Items[0].Id.VideoId
	}

	return tgbotapi.NewMessage(chatID, message)
}

func DefaultHandler(chatID int64) tgbotapi.MessageConfig {
	return tgbotapi.NewMessage(chatID, "Извени, я не могу отвечать на простые сообщения, я пока не умею разговаривать.\n\n"+
		"Воспользуйся одной из команд:\n"+helper.GetCommand())
}
